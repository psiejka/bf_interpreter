import unittest
from bf_interpreter import BFInterpreter


class TestBFInterpreter(unittest.TestCase):
    def test_dataptr_operations(self):
        bf = BFInterpreter([], optimize=True)
        for i in range(4):
            bf.increment_dataptr()
        for i in range(2):
            bf.decrement_dataptr()
        self.assertEqual(bf.dataptr, 2)
        bf = BFInterpreter(['>', '>', '>', '>', '<', '<', ], optimize=True)
        bf.execute()
        self.assertEqual(bf.dataptr, 2)

    def test_memory_operations(self):
        bf = BFInterpreter([], optimize=True)
        for i in range(4):
            bf.increment_memory()
        for i in range(2):
            bf.decrement_memory()
        self.assertEqual(bf.memory[0], 2)
        bf = BFInterpreter(['+', '+', '+', '+', '-', '-', ], optimize=True)
        bf.execute()
        self.assertEqual(bf.memory[0], 2)

    def test_loop(self):
        bf = BFInterpreter(['+', '+', '+', '[', '>', '+', '+', '<', '-', ']'], optimize=True)
        bf.execute()
        self.assertEqual(bf.memory[0], 0)
        self.assertEqual(bf.memory[1], 6)


if __name__ == '__main__':
    unittest.main()
