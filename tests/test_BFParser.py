import unittest
from bf_interpreter import BFParser


class TestBFParser(unittest.TestCase):
    def test_parser(self):
        instructions = BFParser.parse_file("test_parser.bf")
        self.assertEqual(instructions, ['+','+','+','+','>','>','>','>','+','+','+','>','>','[','-',']',']','<','<',])


if __name__ == '__main__':
    unittest.main()
