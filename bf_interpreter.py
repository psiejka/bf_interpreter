import time


def time_measure(decorated):
    def wrapper(*args, **kwargs):
        start = time.time()
        decorated(*args, **kwargs)
        end = time.time()
        print("\nExecution time: ", end - start, "seconds.")
    return wrapper


class BFParser:
    @staticmethod
    def parse_file(filename):
        instructions = []

        with open(filename, 'r') as file:
            for line in file:
                for c in line:
                    if c == '+' or c == '-' or c == '>' or c == '<' or c == '.' or c == ',' or c == '[' or c == ']':
                        instructions.append(c)
        return instructions


class BFInterpreter:
    def __init__(self, instructions, optimize=True):
        self.program_max_size = 30000
        self.pc = 0
        self.dataptr = 0
        self.optimize = optimize
        self.memory = [0]*self.program_max_size  # BF should allocate 30000 bytes of memory
        self.instructions = instructions
        self.brackets_map = [0] * (len(self.instructions))

    def increment_dataptr(self):
        self.dataptr += 1

    def decrement_dataptr(self):
        self.dataptr -= 1

    def increment_memory(self):
        self.memory[self.dataptr] += 1

    def decrement_memory(self):
        self.memory[self.dataptr] -= 1

    def print(self):
        memory = self.memory[self.dataptr]
        print(chr(memory), end='')

    def input(self):
        self.memory[self.dataptr] = int(input())

    def loop_start(self):
        if self.memory[self.dataptr] == 0:
            if not self.optimize:
                bracket_count = 1
                self.pc += 1

                while bracket_count and (self.pc < len(self.instructions)):
                    self.pc += 1
                    if self.instructions[self.pc] == ']':
                        bracket_count -= 1
                    elif self.instructions[self.pc] == '[':
                        bracket_count += 1

                if not bracket_count:
                    return
            else:
                self.pc = self.brackets_map[self.pc]

    def loop_end(self):
        if self.memory[self.dataptr] != 0:
            if not self.optimize:
                bracket_count = 1

                while bracket_count and (self.pc > 0):
                    self.pc -= 1
                    if self.instructions[self.pc] == '[':
                        bracket_count -= 1
                    elif self.instructions[self.pc] == ']':
                        bracket_count += 1

                if not bracket_count:
                    return
            else:
                self.pc = self.brackets_map[self.pc]

    def create_jumptable(self):
        pc = 0
        size = len(self.instructions)
        while pc < size:
            instruction = self.instructions[pc]
            if instruction == '[':
                bracket_count = 1
                seek = pc

                while bracket_count and seek+1 < size:
                    seek += 1
                    if self.instructions[seek] == ']':
                        bracket_count -= 1
                    elif self.instructions[seek] == '[':
                        bracket_count += 1

                if not bracket_count:
                    self.brackets_map[seek] = pc
                    self.brackets_map[pc] = seek
            pc += 1

    @time_measure
    def execute(self):
        switch = {
            '>': self.increment_dataptr,
            '<': self.decrement_dataptr,
            '+': self.increment_memory,
            '-': self.decrement_memory,
            '.': self.print,
            ',': self.input,
            '[': self.loop_start,
            ']': self.loop_end,
        }

        if self.optimize:
            self.create_jumptable()

        while self.pc < len(self.instructions):
            instruction = self.instructions[self.pc]
            func = switch.get(instruction, lambda: "INVALID SIGN")
            func()
            self.pc += 1


if __name__ == "__main__":
    loaded_instructions = BFParser.parse_file("fibonacci.bf")

    bf = BFInterpreter(loaded_instructions, optimize=True)
    bf.execute()

